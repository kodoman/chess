﻿
public enum ChessPieceType {

    Test,
	Pawn,
    Knight,
    Bishop,
    Rook,
    Queen,
    King
}
