﻿public enum MoveType
{
    Move,
    Eat,
    Castling
}

public static class MoveTypHelpers
{
    public static string GetDescription(this MoveType moveType)
    {
        switch (moveType)
        {
            case MoveType.Castling:
                return "castling";
            case MoveType.Eat:
                return "eating using";
            default:
                return "moving";
        }
    }
}