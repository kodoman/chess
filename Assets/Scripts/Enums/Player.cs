﻿public enum Player {
    
	White,
    Black
}

public static class PlayerFunctions
{
    public static Player GetOpponent(this Player player)
    {
        return player == Player.White 
            ? Player.Black 
            : Player.White;
    }
}