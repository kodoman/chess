﻿using System;
using UnityEngine;

public static class ScriptChecker
{

    public static T GetScript<T>(this GameObject obj)
    {
        var script = obj.GetComponent<T>();

        if (script == null)
        {
            throw new Exception(obj.GetType().Name + " " + obj.name + " has no " + typeof(T).Name + " script!");
        }

        return script;
    }

    public static T GetRequiredComponent<T>(this Component obj)
    {
        var script = obj.GetComponent<T>();

        if (script == null)
        {
            throw new Exception(obj.GetType().Name + " " + obj.name + " has no component of type " + typeof(T).Name + "!");
        }

        return script;
    }
}