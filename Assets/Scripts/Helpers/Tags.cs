﻿
public static class Tags  {

    public static string Board
    {
        get { return "Board"; }
    }

    public static string PiecesParent
    {
        get { return "PiecesParent"; }
    }

    public static string EatenPiecesWhite
    {
        get { return "EatenPiecesWhite"; }
    }
    public static string EatenPiecesBlack
    {
        get { return "EatenPiecesBlack"; }
    }
}
