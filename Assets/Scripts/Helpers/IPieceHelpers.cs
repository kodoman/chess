﻿using System;
using UnityEngine;

public static class IPieceHelpers
{
    public static Vector2Int GetBoardCoords(this IPiece piece)
    {
        var currentTile = piece.CurrentTile;

        if (currentTile == null)
            throw new ApplicationException("Trying to get board coords for a piece that is not on board!");

        return currentTile.GetScript<Tile>().XBoardCoords;
    }
}
