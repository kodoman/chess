﻿using UnityEngine;


public static class Vector2IntHelpers
{
    private static string[] XCoordNames = {"A", "B", "C", "D", "E", "F", "G", "H"};
    private static string[] YCoordNames = {"1", "2", "3", "4", "5", "6", "7", "8"};
    private static string Unknown = "Unknown";

    public static string GetBoardName(this Vector2Int boardCoords)
    {
        if (boardCoords.x > XCoordNames.Length - 1 || boardCoords.y > YCoordNames.Length - 1)
            return Unknown;

        return XCoordNames[boardCoords.x] + YCoordNames[boardCoords.y];
    }
}
