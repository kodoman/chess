﻿using System.Collections.Generic;
using UnityEngine;

public interface IPiece {
    
    ChessPieceType ChessPieceType { get; }
    Player Player { get; set; }
    GameObject CurrentTile { get; set; }
    bool HasMoved { get; set; }
    bool IsInPlay { get; set; }

    void Mark();
    void Unmark();
    List<Move> GetAllowedMoves(IBoardReadonly<IPiece> pieces);
}
