﻿using System.Collections.Generic;
using UnityEngine;

public interface IBoardReadonly<T>
{
    int Size { get; }

    T Get(Vector2Int boardCoords);
    bool IsOnPlayingField(Vector2Int boardCoords);
    List<T> GetAll();
    Board<T> MakeEditableCopy();
}
