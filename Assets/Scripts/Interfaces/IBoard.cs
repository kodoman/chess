﻿using UnityEngine;

public interface IBoard<T> : IBoardReadonly<T>
{
    void Set(Vector2Int boardCoords, T value);
}
