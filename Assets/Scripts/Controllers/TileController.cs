﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class TileController : MonoBehaviour
{
    public GameObject WhiteTile;
    public GameObject BlackTile;

    [HideInInspector]
    public TileObjectBoard TileObjectBoard;

    private GameObject _whiteSpawnPoint;
    private GameObject _blackSpawnPoint;

    private PieceController _pieceController;

    private void Awake()
    {
        _whiteSpawnPoint = GameObject.FindGameObjectWithTag(Tags.EatenPiecesWhite);
        _blackSpawnPoint = GameObject.FindGameObjectWithTag(Tags.EatenPiecesBlack);

        _pieceController = this.GetRequiredComponent<PieceController>();
    }

    public void PrepareBoard(int boardSize)
    {
        GenerateBoard(boardSize);
        SetSpawnPoints(boardSize);
    }

    public void UnmarkAllTiles()
    {
        UnmarkAllTilesInternal();
    }

    public void MarkTiles(List<Move> movesToMark)
    {
        UnmarkAllTilesInternal();

        foreach (var move in movesToMark)
        {
            var tile = TileObjectBoard.GetTile(move.Destination);

            tile.SetMarked(true, move.MoveType);
        }
    }
    
    public Vector3 GetSpawnPoint(Player player)
    {
        var eatenPiecesCount = _pieceController.GetAllPieces()
            .Count(x => x.Player == player.GetOpponent() && !x.IsInPlay);

        var deltaX = eatenPiecesCount * WhiteTile.transform.localScale.x / 2;

        if (player == Player.White)
        {
            var location = _whiteSpawnPoint.transform.position;
            return new Vector3(location.x + deltaX, location.y, location.z);
        }
        else
        {
            var location = _blackSpawnPoint.transform.position;
            return new Vector3(location.x - deltaX, location.y, location.z);
        }
    }

    public Vector3 GetPositionForTile(Vector2Int boardCoords)
    {
        return GetPositionForTileInternal(boardCoords);
    }

    private void UnmarkAllTilesInternal()
    {
        for (var i = 0; i < TileObjectBoard.Size; i++)
        {
            for (var j = 0; j < TileObjectBoard.Size; j++)
            {
                var tile = TileObjectBoard.GetTile(new Vector2Int(i, j));
                tile.SetMarked(false);
            }
        }
    }

    private void GenerateBoard(int boardSize)
    {
        TileObjectBoard = new TileObjectBoard(boardSize);

        var boardObj = GameObject.FindGameObjectWithTag("Board");

        var colStartsWithWhite = false;

        for (var col = 0; col < boardSize; col++)
        {
            var tileIsWhite = colStartsWithWhite;

            for (var row = 0; row < boardSize; row++)
            {
                var position = GetPositionForTileInternal(new Vector2Int(col, row));

                var tile = Instantiate(
                    tileIsWhite
                        ? WhiteTile
                        : BlackTile,
                    boardObj.transform);

                tile.transform.position = position;

                var tileScript = tile.GetScript<Tile>();

                var boardCoords = new Vector2Int(col, row);

                tileScript.XBoardCoords = boardCoords;

                TileObjectBoard.Set(boardCoords, tile);


                tileIsWhite = !tileIsWhite;
            }

            colStartsWithWhite = !colStartsWithWhite;
        }

    }

    private void SetSpawnPoints(int boardSize)
    {
        var whiteSpawnPoint = GameObject.FindGameObjectWithTag(Tags.EatenPiecesWhite);
        var blackSpawnPoint = GameObject.FindGameObjectWithTag(Tags.EatenPiecesBlack);

        whiteSpawnPoint.transform.position = GetPositionForTileInternal(new Vector2Int(0, -1));
        blackSpawnPoint.transform.position = GetPositionForTileInternal(new Vector2Int(boardSize - 1, boardSize));
    }

    private Vector3 GetPositionForTileInternal(Vector2Int boardCoords)
    {
        // assuming board size is an even number

        var lowerLeftFieldCoordinateX = - (TileObjectBoard.Size / 2 - 0.5f) * WhiteTile.transform.localScale.x;
        var lowerLeftFieldCoordinateZ = - (TileObjectBoard.Size / 2 - 0.5f) * WhiteTile.transform.localScale.z;

        return new Vector3(
            lowerLeftFieldCoordinateX + boardCoords.x * WhiteTile.transform.localScale.x,
            0,
            lowerLeftFieldCoordinateZ + boardCoords.y * WhiteTile.transform.localScale.z);
    }
}
