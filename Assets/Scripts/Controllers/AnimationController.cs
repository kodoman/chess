﻿using System.Collections;
using UnityEngine;

public class AnimationController : MonoBehaviour {

    public float RaiseHeight;
    public float RaiseAnimationSeconds;
    public float TravelAnimationSeconds;

    public IEnumerator RaiseAnimation(GameObject obj)
    {
        var position = obj.transform.position;

        yield return AnimatePosition(obj.transform, position, new Vector3(position.x, RaiseHeight, position.z), RaiseAnimationSeconds);
    }

    public IEnumerator TravelAnimation(GameObject obj, GameObject tile)
    {
        var position = obj.transform.position;

        var tilePosition = tile.transform.position;

        yield return AnimatePosition(obj.transform, position, new Vector3(tilePosition.x, position.y, tilePosition.z), RaiseAnimationSeconds);
    }

    public IEnumerator LowerAnimation(GameObject obj, GameObject tile)
    {
        var position = obj.transform.position;

        var tilePosition = tile.transform.position;

        var destinationHeight = tile.GetComponent<Tile>().TileHeight;

        yield return AnimatePosition(obj.transform, position, new Vector3(tilePosition.x, destinationHeight, tilePosition.z), RaiseAnimationSeconds);
    }

    IEnumerator AnimatePosition(Transform obj, Vector3 startPosition, Vector3 endPosition, float duration)
    {
        var animationStartTime = Time.time;

        var difference = endPosition - startPosition;

        var animationTimeDoneNormalized = 0.0f;

        // if == 1, only time is done, but not the animation
        while (animationTimeDoneNormalized <= 1)
        {
            animationTimeDoneNormalized = (Time.time - animationStartTime) / duration;

            var newPositionDiff = difference * animationTimeDoneNormalized;

            var newPosition = startPosition + newPositionDiff;

            obj.transform.position = newPosition;

            yield return null;
        }
    }
}
