﻿using UnityEngine;

public class SelectionController : MonoBehaviour
{
    public Player XPlayerInTurn;

    private GameObject SelectedObject { get; set; }

    private MovementController _movementController;
    private TileController _tileController;
    private UiController _uiController;
    private SelectionState _selectionState = SelectionState.None;

    public void ToggleUnitSelecting(bool active)
    {
        _selectionState = active
            ? SelectionState.UnitOrDestination
            : SelectionState.None;
    }

    private void Awake()
    {
        _movementController = this.GetRequiredComponent<MovementController>();
        _uiController = this.GetRequiredComponent<UiController>();
        _tileController = this.GetRequiredComponent<TileController>();
    }

    private void Update ()
    { 
        MouseSelect(XPlayerInTurn);
    }

    private void MouseSelect(Player playerInTurn)
    {
        if (_selectionState == SelectionState.None)
            return;

        if (Input.GetMouseButtonDown(0))
        {
            if (SelectedObject == null)
            {
                SelectUnit(playerInTurn);
            }
            else
            {
                if (CanSelectChessPiece(GetClickedChessPiece(), playerInTurn))
                    SelectUnit(playerInTurn);
                else
                    MoveSelectedPiece();
            }
        }
    }

    private void SelectUnit(Player playerInTurn)
    {
        var clickedObject = GetClickedChessPiece();

        GameObject selectedObject = null;

        if (CanSelectChessPiece(clickedObject, playerInTurn))
            selectedObject = clickedObject;

        SelectObject(selectedObject);

        if (selectedObject != null)
        {
            var pieceScript = selectedObject.GetScript<IPiece>();

            pieceScript.Mark();

            _movementController.MarkAvailableMoves(pieceScript);
        }
        else
        {
            _movementController.ClearAvailableMoves();
        }
    }

    private void MoveSelectedPiece()
    {
        if (SelectedObject == null)
            return;

        var clickedTile = GetClickedObject(Layers.Ground);

        if (clickedTile == null)
            return;

        var destinationBoardCoords = clickedTile.GetScript<Tile>().XBoardCoords;

        SelectObject(null);

        _movementController.ExecuteTurn(destinationBoardCoords);

        _tileController.UnmarkAllTiles();
    }

    private GameObject GetClickedChessPiece()
    {
        var clickedTile = GetClickedObject(Layers.Ground);

        if (clickedTile == null) return null;

        var clickedObject = clickedTile.GetScript<Tile>().XUnitOnTile;

        return clickedObject;
    }

    private bool CanSelectChessPiece(GameObject obj, Player? allowedPlayer = null)
    {
        if (obj == null) return false;

        var script = obj.GetScript<IPiece>();

        var result = script.IsInPlay;

        if (allowedPlayer == null)
            return result;

        return script.Player == allowedPlayer.Value;
    }

    void SelectObject(GameObject obj)
    {
        DeselectPiece(SelectedObject);

        SelectedObject = obj;
        _uiController.SetSelectText(obj);
    }

    private void DeselectPiece(GameObject piece)
    {
        if (piece == null) return;

        var currentPiece = SelectedObject.GetComponent<IPiece>();

        if (currentPiece != null) currentPiece.Unmark();
    }

    GameObject GetClickedObject(string layerName)
    {
        RaycastHit hit;

        var isSomethingHit = Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit, 1000,
            LayerMask.GetMask(layerName));

        if (!isSomethingHit)
            return null;

        if (hit.transform == null)
            return null;

        return hit.transform.gameObject;
    }
}
