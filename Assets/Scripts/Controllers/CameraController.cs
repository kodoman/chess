﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public Color WhiteBackgroundColor = Color.white;
    public Color BlackBackgroundColor = Color.black;

    private Camera _mainCamera;
    private Animation _cameraAnimation;

    private Vector3 _defaultCameraPosition;
    private Quaternion _defaultCameraRotation;
    private Vector3 _invertedCameraPosition;
    private Quaternion _invertedCameraRotation;

    public void Awake()
    {
        _mainCamera = Camera.main;

        _defaultCameraPosition = _mainCamera.transform.position;
        _defaultCameraRotation = _mainCamera.transform.rotation;

        _invertedCameraPosition = new Vector3(
            _defaultCameraPosition.x,
            _defaultCameraPosition.y,
            -_defaultCameraPosition.z);

        var defaultRotationEuler = _defaultCameraRotation.eulerAngles;

        _invertedCameraRotation = Quaternion.Euler(new Vector3(
            defaultRotationEuler.x, 
            defaultRotationEuler.y + 180,
            defaultRotationEuler.z));
    }

    public void SetCameraColor(Player player)
    {
        Camera.main.backgroundColor =
            player == Player.White
            ? WhiteBackgroundColor
            : BlackBackgroundColor;
    }

    public void SetCameraForPlayer(Player player)
    {
        //_cameraAnimation.Play();

        if (player == Player.White)
        {
            _mainCamera.transform.position = _defaultCameraPosition;
            _mainCamera.transform.rotation = _defaultCameraRotation;
        }
        else
        {
            _mainCamera.transform.position = _invertedCameraPosition;
            _mainCamera.transform.rotation = _invertedCameraRotation;
        }
    }
}
