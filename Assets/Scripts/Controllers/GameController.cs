﻿using UnityEngine;

public class GameController : MonoBehaviour
{

    public Player PlayerInTurn = Player.White;
    public const int BoardSize = 8;

    private TileController _tileController;
    private PieceController _pieceController;
    private SelectionController _selectionController;
    private UiController _uiController;
    private CameraController _cameraController;
    
    void Start ()
    {
        _pieceController = this.GetRequiredComponent<PieceController>();
        _tileController = this.GetRequiredComponent<TileController>();
        _selectionController = this.GetRequiredComponent<SelectionController>();
        _uiController = this.GetRequiredComponent<UiController>();
        _cameraController = this.GetRequiredComponent<CameraController>();
    }

    public void PrepareGame()
    {
        _tileController.PrepareBoard(BoardSize);
        _pieceController.GeneratePieces(BoardSize);
        //_pieceController.GeneratePawnsToPromote();
        //_pieceController.GenerateEndgame();
    }

    public void StartGame(Player startingPlayer = Player.White)
    {
        PlayerInTurn = startingPlayer;
        UpdatePlayer();
        _selectionController.ToggleUnitSelecting(true);
    }

    public void TearDownGame()
    {
        var board = GameObject.FindGameObjectWithTag(Tags.Board);

        foreach (Transform tile in board.transform)
        {
            Destroy(tile.gameObject);
        }

        var piecesParent = GameObject.FindGameObjectWithTag(Tags.PiecesParent);

        foreach (Transform piece in piecesParent.transform)
        {
            Destroy(piece.gameObject);
        }

        _pieceController.ResetPiecesReference();
    }

    public void FinishTurn()
    {
        OutputCheckStatus();

        PlayerInTurn = PlayerInTurn.GetOpponent();
        UpdatePlayer();

        var pieces = _tileController.TileObjectBoard.PieceBoard;

        var hasAvailableMoves = BoardAnalyzer.HasAvailableMoves(pieces, PlayerInTurn);

        if (!hasAvailableMoves && BoardAnalyzer.IsKingInCheck(pieces, PlayerInTurn))
        {
            FinishGameVictory(PlayerInTurn.GetOpponent());
        }
        else if (!hasAvailableMoves)
        {
            FinishGameDraw();
        }
    }

    private void UpdatePlayer()
    {
        _selectionController.XPlayerInTurn = PlayerInTurn;
        _uiController.SetPlayerText(PlayerInTurn);

        _cameraController.SetCameraColor(PlayerInTurn);
        _cameraController.SetCameraForPlayer(PlayerInTurn);
    }

    private void OutputCheckStatus()
    {
        var isWhiteInCheck = BoardAnalyzer.IsKingInCheck(_tileController.TileObjectBoard.PieceBoard, Player.White);

        var isBlackInCheck = BoardAnalyzer.IsKingInCheck(_tileController.TileObjectBoard.PieceBoard, Player.Black);

        _uiController.SetCheckText(isWhiteInCheck, isBlackInCheck);
    }

    private void FinishGameVictory(Player winner)
    {
        _uiController.SetFinishGameText(winner);
        _uiController.SetFinishGameMenuVisibility(true);
        WrapUpGame();
    }

    private void FinishGameDraw()
    {
        _uiController.SetFinishGameText(null);
        _uiController.SetFinishGameMenuVisibility(true);
        WrapUpGame();
    }

    private void WrapUpGame()
    {
        _selectionController.ToggleUnitSelecting(false);
    }
}
