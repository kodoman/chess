﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ApplicationController : MonoBehaviour
{

    private UiController _uiController;
    private GameController _gameController;

    public void Awake()
    {
        _uiController = this.GetRequiredComponent<UiController>();
        _gameController = this.GetRequiredComponent<GameController>();
    }

    public void Start()
    {
        _uiController.SetGameMenuVisibility(true);
    }

    public void NewGame()
    {
        _uiController.SetGameMenuVisibility(false);
        _gameController.PrepareGame();
        _gameController.StartGame();
    }

    public void ExitGame()
    {
        _gameController.TearDownGame();
        _uiController.SetFinishGameMenuVisibility(false);
        _uiController.SetGameMenuVisibility(true);
    }

    public void Quit()
    {
        Application.Quit();
    }
}
