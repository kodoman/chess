﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class MovementController : MonoBehaviour
{
    private TileController _tileController;
    private SelectionController _selectionController;
    private AnimationController _animationController;
    private PieceController _pieceController;
    private GameController _gameController;
    private UiController _uiController;

    private Move _pendingMove;

    private IDictionary<Vector2Int, Move> _moveBoard;
    private static readonly Dictionary<Vector2Int, Move> EmptyMoveBoard = new Dictionary<Vector2Int, Move>();

    private void Awake()
    {
        _tileController = this.GetRequiredComponent<TileController>();
        _animationController = this.GetRequiredComponent<AnimationController>();
        _selectionController = this.GetRequiredComponent<SelectionController>();
        _pieceController = this.GetRequiredComponent<PieceController>();
        _gameController = this.GetRequiredComponent<GameController>();
        _uiController = this.GetRequiredComponent<UiController>();
    }

    // most of this stuff should bee in a separate MoveController rather than a MovementController
    public void MarkAvailableMoves(IPiece piece)
    {
        var availableTiles = BoardAnalyzer.GetAllowedMoves(piece, _tileController.TileObjectBoard.PieceBoard);

        _moveBoard = availableTiles;

        // here we can mark the tiles differently based on the move type
        _tileController.MarkTiles(availableTiles.Values.ToList());
    }

    public void ClearAvailableMoves()
    {
        _moveBoard = EmptyMoveBoard;

        _tileController.UnmarkAllTiles();
    }

    public void ExecuteTurn(Vector2Int destinationBoardCoords)
    {
        if (!_moveBoard.ContainsKey(destinationBoardCoords))
            return;

        var move = _moveBoard[destinationBoardCoords];

        if (move == null)
        {
            Debug.Log("Trying to execute a null move.");
            return;
        }

        _uiController.SetMoveText(move);

        _selectionController.ToggleUnitSelecting(false);

        if (move.CanPromotePawn())
        {
            _pieceController.PawnToPromote = _tileController.TileObjectBoard.Get(move.Source);

            _uiController.SetPawnPromoteMenuVisibility(true);

            _pendingMove = move;

            return;
        }

        StartCoroutine(ExecuteTurnCoroutine(move));
    }

    public void PlaceOnTile(GameObject obj, GameObject tile)
    {
        PlaceOnTileInternal(obj, tile);
    }

    public void PromotePawnToQueen()
    {
        var pawnToPromote = _tileController.TileObjectBoard.Get(_pendingMove.Source)
            .GetScript<Tile>()
            .XUnitOnTile;

        _pieceController.ReplacePiece(pawnToPromote, ChessPieceType.Queen);

        ContinuePendingMove();
    }
    public void PromotePawnToRook()
    {
        var pawnToPromote = _tileController.TileObjectBoard.Get(_pendingMove.Source)
            .GetScript<Tile>()
            .XUnitOnTile; ;

        _pieceController.ReplacePiece(pawnToPromote, ChessPieceType.Rook);

        ContinuePendingMove();
    }
    public void PromotePawnToBishop()
    {
        var pawnToPromote = _tileController.TileObjectBoard.Get(_pendingMove.Source)
            .GetScript<Tile>()
            .XUnitOnTile; ;

        _pieceController.ReplacePiece(pawnToPromote, ChessPieceType.Bishop);

        ContinuePendingMove();
    }
    public void PromotePawnToKnight()
    {
        var pawnToPromote = _tileController.TileObjectBoard.Get(_pendingMove.Source)
            .GetScript<Tile>()
            .XUnitOnTile; ;

        _pieceController.ReplacePiece(pawnToPromote, ChessPieceType.Knight);

        ContinuePendingMove();
    }

    private void ContinuePendingMove()
    {
        _uiController.SetPawnPromoteMenuVisibility(false);

        StartCoroutine(ExecuteTurnCoroutine(_pendingMove));

        _pendingMove = null;
    }

    private IEnumerator ExecuteTurnCoroutine(Move move)
    {
        yield return ExecuteSingleMoveCoroutine(move);

        _selectionController.ToggleUnitSelecting(true);

        _gameController.FinishTurn();

        yield return null;
    }

    private IEnumerator ExecuteSingleMoveCoroutine(Move move)
    {
        var sourceTileObj = _tileController.TileObjectBoard.Get(move.Source);

        var destinationTileObj = _tileController.TileObjectBoard.Get(move.Destination);

        var pieceObj = sourceTileObj.GetScript<Tile>().XUnitOnTile;

        yield return MoveToTileCoroutine(pieceObj, destinationTileObj);

        if (move.CastlingOtherMove != null)
            yield return ExecuteSingleMoveCoroutine(move.CastlingOtherMove);
    }
    
    // the MoveToTile animation can be replaced with just setting the coordinates if no animation is needed
    // but that needs to be tested
    private IEnumerator MoveToTileCoroutine(GameObject obj, GameObject tile)
    {
        obj.GetScript<IPiece>().HasMoved = true;

        var existingPieceOnTile = tile.GetScript<Tile>().XUnitOnTile;

        if(existingPieceOnTile != null)
            _pieceController.RemoveFromGame(existingPieceOnTile);

        PlaceOnTileInternal(obj, tile);

        yield return MoveToTileAnimation(obj, tile, existingPieceOnTile);

        yield return null;
    }

    IEnumerator MoveToTileAnimation(GameObject obj, GameObject tile, GameObject existingPieceOnTile)
    {
        var tileLocation = tile.transform.position;

        yield return _animationController.RaiseAnimation(obj);

        yield return _animationController.TravelAnimation(obj, tile);

        // the travel animation overshoots - this is the correction
        obj.transform.position = new Vector3(tileLocation.x, obj.transform.position.y, tileLocation.z);
        
        if (existingPieceOnTile != null)
        {
            existingPieceOnTile.transform.position = _tileController.GetSpawnPoint(obj.GetScript<IPiece>().Player);
        }

        yield return _animationController.LowerAnimation(obj, tile);
    }

    // should this also be moved to one of the Board classes, since it sets the references?
    private void PlaceOnTileInternal(GameObject obj, GameObject tile)
    {
        var pieceScript = obj.GetScript<IPiece>();
        var tileScript = tile.GetScript<Tile>();

        if (pieceScript.CurrentTile != null)
        {
            // using GetComponent instead of GetScript because the piece may be on no tile
            var currentTileScript = pieceScript.CurrentTile.GetComponent<Tile>();

            if (currentTileScript != null)
            {
                currentTileScript.XUnitOnTile = null;
            }
        }

        pieceScript.CurrentTile = tile;
        tileScript.XUnitOnTile = obj;
    }
}
