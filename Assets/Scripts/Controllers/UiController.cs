﻿using System.Text;
using UnityEngine;
using UnityEngine.UI;

public class UiController : MonoBehaviour {
    
    public Text SelectedObjectText;
    public Text StatusText;
    public Text PlayerText;
    public Text GameOverText;
    // public Image GameOverTextBackground;
    public Canvas GameMenu;
    public Canvas GameOverMenu;
    public Canvas PawnPromoteMenu;

    private void Awake()
    {
        SelectedObjectText.text = "";
        StatusText.text = "";
        PlayerText.text = "";
        // GameOverText.text = "";
        SetFinishGameMenuVisibilityInternal(false);
    }

    public void SetGameMenuVisibility(bool isVisible)
    {
        SetGameMenuVisibilityInternal(isVisible);
    }

    public void SetPawnPromoteMenuVisibility(bool isVisible)
    {
        SetPawnPromoteMenuVisibilityInternal(isVisible);
    }

    public void SetSelectText(GameObject selectedObject)
    {
        if (selectedObject == null)
            SelectedObjectText.text = "";
        else
            SelectedObjectText.text = selectedObject.name + " selected";
    }

    // reusing the selectedobjecttext not to cramp the screen with letters
    public void SetMoveText(Move move)
    {
        if (move == null)
            SelectedObjectText.text = "";
        else
            SelectedObjectText.text = new StringBuilder()
                .Append(move.Player)
                .Append(" ")
                .Append(move.MoveType.GetDescription())
                .Append(" ")
                .Append(move.PieceType)
                .Append(" from ")
                .Append(move.Source.GetBoardName())
                .Append(" to ")
                .Append(move.Destination.GetBoardName())
                .ToString();
    }

    public void SetPlayerText(Player player)
    {
        PlayerText.text = "Player in turn : " + player;
    }

    public void SetFinishGameText(Player? winner)
    {
        if (winner == null)
            GameOverText.text = "Draw!";
        else
            GameOverText.text = winner.Value + " wins!";

        SelectedObjectText.text = "";
        PlayerText.text = "";
        StatusText.text = "";
    }

    public void SetFinishGameMenuVisibility(bool isVisible)
    {
        SetFinishGameMenuVisibilityInternal(isVisible);
    }

    // both kings should never be in check simultaneously, but who knows
    public void SetCheckText(bool isWhiteInCheck, bool isBlackInCheck)
    {
        var textBuilder = new StringBuilder();

        if (isWhiteInCheck)
            textBuilder.Append("White king in check!");
        if (isBlackInCheck)
            textBuilder.Append("Black king in check!");

        StatusText.text = textBuilder.ToString();
    }

    private void SetGameMenuVisibilityInternal(bool isVisible)
    {
        GameMenu.enabled = isVisible;
    }

    private void SetPawnPromoteMenuVisibilityInternal(bool isVisible)
    {
        PawnPromoteMenu.enabled = isVisible;
    }

    private void SetFinishGameMenuVisibilityInternal(bool isVisible)
    {
        GameOverMenu.enabled = isVisible;
    }
}