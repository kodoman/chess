﻿using System.Collections.Generic;
using UnityEngine;

public class PieceController : MonoBehaviour
{ 
    public GameObject PawnWhite;
    public GameObject KnightWhite;
    public GameObject BishopWhite;
    public GameObject RookWhite;
    public GameObject QueenWhite;
    public GameObject KingWhite;

    public GameObject PawnBlack;
    public GameObject KnightBlack;
    public GameObject BishopBlack;
    public GameObject RookBlack;
    public GameObject QueenBlack;
    public GameObject KingBlack;

    public GameObject PawnToPromote;

    private TileController _tileController;
    private MovementController _movementController;
    private UiController _uiController;

    private List<IPiece> _pieces;

    private GameObject _piecesParent;

    private void Awake()
    {
        _tileController = this.GetRequiredComponent<TileController>();
        _movementController = this.GetRequiredComponent<MovementController>();
        _uiController = this.GetRequiredComponent<UiController>();

        if(_pieces == null)
            ResetPiecesReference();

        _piecesParent = GameObject.FindGameObjectWithTag(Tags.PiecesParent);
    }

    public List<IPiece> GetAllPieces()
    {
        return _pieces;
    }

    public void ResetPiecesReference()
    {
        _pieces = new List<IPiece>();
    }

    public void GeneratePieces(int boardSize)
    {
        for (var col = 0; col < boardSize; col++)
        {
            GeneratePiece(PawnWhite, new Vector2Int(col, 1), Player.White);
        }

        GeneratePiece(RookWhite, new Vector2Int(0, 0), Player.White);
        GeneratePiece(RookWhite, new Vector2Int(boardSize - 1, 0), Player.White);

        GeneratePiece(KnightWhite, new Vector2Int(1, 0), Player.White);
        GeneratePiece(KnightWhite, new Vector2Int(6, 0), Player.White);

        GeneratePiece(BishopWhite, new Vector2Int(2, 0), Player.White);
        GeneratePiece(BishopWhite, new Vector2Int(5, 0), Player.White);

        GeneratePiece(QueenWhite, new Vector2Int(3, 0), Player.White);
        GeneratePiece(KingWhite, new Vector2Int(4, 0), Player.White);

        for (var col = 0; col < boardSize; col++)
        {
            GeneratePiece(PawnBlack, new Vector2Int(col, 6), Player.Black);
        }

        GeneratePiece(RookBlack, new Vector2Int(0, 7), Player.Black);
        GeneratePiece(RookBlack, new Vector2Int(7, 7), Player.Black);

        GeneratePiece(KnightBlack, new Vector2Int(1,7), Player.Black);
        GeneratePiece(KnightBlack, new Vector2Int(6, 7), Player.Black);

        GeneratePiece(BishopBlack, new Vector2Int(2, 7), Player.Black);
        GeneratePiece(BishopBlack, new Vector2Int(5, 7), Player.Black);

        GeneratePiece(QueenBlack, new Vector2Int(4, 7), Player.Black);
        GeneratePiece(KingBlack, new Vector2Int(3, 7), Player.Black);
    }

    public void GenerateEndgame()
    {
        GeneratePiece(KingWhite, new Vector2Int(0, 6), Player.White);

        GeneratePiece(QueenBlack, new Vector2Int(3, 5), Player.Black);
        GeneratePiece(KingBlack, new Vector2Int(2, 7), Player.Black);
    }

    public void GeneratePawnsToPromote()
    {
        GeneratePiece(KingWhite, new Vector2Int(0, 0), Player.White);

        GeneratePiece(KingBlack, new Vector2Int(0, 7), Player.Black);

        for (var col = 3; col < 8; col++)
        {
            GeneratePiece(PawnWhite, new Vector2Int(col, 6), Player.White);
        }

        for (var col = 3; col < 8; col++)
        {
            GeneratePiece(PawnBlack, new Vector2Int(col, 1), Player.Black);
        }
    }

    //public void PromotePawnToQueen()
    //{
    //    ReplacePiece(PawnToPromote, ChessPieceType.Queen);

    //    PawnToPromote = null;

    //    _uiController.SetPawnPromoteMenuVisibility(false);
    //}
    //public void PromotePawnToRook()
    //{
    //    ReplacePiece(PawnToPromote, ChessPieceType.Rook);

    //    PawnToPromote = null;

    //    _uiController.SetPawnPromoteMenuVisibility(false);
    //}
    //public void PromotePawnToBishop()
    //{
    //    ReplacePiece(PawnToPromote, ChessPieceType.Bishop);

    //    PawnToPromote = null;

    //    _uiController.SetPawnPromoteMenuVisibility(false);
    //}
    //public void PromotePawnToKnight()
    //{
    //    ReplacePiece(PawnToPromote, ChessPieceType.Knight);

    //    PawnToPromote = null;

    //    _uiController.SetPawnPromoteMenuVisibility(false);
    //}

    public void ReplacePiece(GameObject currentPiece, ChessPieceType newPieceType)
    {
        var currentPieceScript = currentPiece.GetScript<IPiece>();

        var piecePrefab = GetPiecePrefab(newPieceType, currentPieceScript.Player);

        var newPiece = GeneratePiece(piecePrefab, currentPieceScript.GetBoardCoords(), currentPieceScript.Player);

        var newPieceScript = newPiece.GetScript<IPiece>();

        newPieceScript.HasMoved = true;

        _pieces.Remove(currentPieceScript);

        Destroy(currentPiece);
    }

    public void RemoveFromGame(GameObject piece)
    {
        if (piece == null)
            return;

        var pieceScript = piece.GetScript<IPiece>();

        pieceScript.CurrentTile.GetScript<Tile>().XUnitOnTile = null;
        pieceScript.CurrentTile = null;

        pieceScript.IsInPlay = false;
    }

    private GameObject GeneratePiece(GameObject piecePrefab, Vector2Int boardCoords, Player player)
    {
        var piece = Instantiate(piecePrefab, _tileController.GetPositionForTile(boardCoords), piecePrefab.transform.rotation, _piecesParent.transform);

        piece.name = piecePrefab.name;

        var pieceScript = piece.GetScript<IPiece>();
        
        var currentTile = _tileController.TileObjectBoard.Get(boardCoords);

        pieceScript.CurrentTile = currentTile;
        pieceScript.Player = player;
        pieceScript.IsInPlay = true;

        _pieces.Add(pieceScript);

        _movementController.PlaceOnTile(piece, currentTile);

        return piece;
    }

    private GameObject GetPiecePrefab(ChessPieceType pieceType, Player player)
    {
        switch (pieceType)
        {
            case ChessPieceType.King:
                return player == Player.Black
                    ? KingBlack
                    : KingWhite;
            case ChessPieceType.Queen:
                return player == Player.Black
                    ? QueenBlack
                    : QueenWhite;
            case ChessPieceType.Rook:
                return player == Player.Black
                    ? RookBlack
                    : RookWhite;
            case ChessPieceType.Bishop:
                return player == Player.Black
                    ? BishopBlack
                    : BishopWhite;
            case ChessPieceType.Knight:
                return player == Player.Black
                    ? KnightBlack
                    : KnightWhite;
            case ChessPieceType.Pawn:
                return player == Player.Black
                    ? PawnBlack
                    : PawnWhite;
            default:
                return null;
        }
    }
}
