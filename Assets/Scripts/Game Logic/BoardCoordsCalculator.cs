﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using UnityEngine;

public static class BoardCoordsCalculator {

    // maybe for castling this shouldn't return moves, but something else?
    // the 2 coord parameters are kind of stupid
    // these are returned in order from the piece's nearest to the furthest relative tile
    public static List<Move> GetAllowedMovesInDirection(Vector2Int originalCoords, Vector2Int boardCoords, Direction direction, IBoardReadonly<IPiece> pieces,
        Player player, ChessPieceType pieceType, int maxLength = -1, bool ignoreEmptyFields = false, bool disallowEnemyFields = false, bool incudeFriendlyRooks = false)
    {
        var emptyList = new List<Move>();

        if (maxLength == 0)
            return emptyList;

        var nextTileCoords = GetBoardCoordsInDirection(boardCoords, direction);

        if(!pieces.IsOnPlayingField(nextTileCoords))
            return emptyList;

        var nextPiece = pieces.Get(nextTileCoords);

        if (nextPiece == null)
        {
            if (ignoreEmptyFields)
                return emptyList;

            var furtherAllowedMoves = GetAllowedMovesInDirection(originalCoords, nextTileCoords, direction, pieces, player, pieceType,
                maxLength - 1, disallowEnemyFields: disallowEnemyFields, incudeFriendlyRooks: incudeFriendlyRooks);

            // merging the list like this so the coordinates will be ordered from the original piece outwards - important for castling
            var result = new List<Move> { new Move(player, pieceType, originalCoords, nextTileCoords) }
            .Union(furtherAllowedMoves)
            .ToList();

            return result;
        }

        if (nextPiece.Player == player && nextPiece.ChessPieceType == ChessPieceType.Rook)
        {
            if (incudeFriendlyRooks)
                return new List<Move> { new Move(player, pieceType, originalCoords, nextTileCoords, MoveType.Castling) };

            else return emptyList;
        }
        else if (nextPiece.Player == player.GetOpponent() && !disallowEnemyFields)
        {
            return new List<Move> { new Move(player, pieceType, originalCoords, nextTileCoords, MoveType.Eat) };
        }

        return emptyList;
    }

    public static List<Move> GetAllowedLShapedMoves(Vector2Int pieceBoardCoords,
        Player player, ChessPieceType pieceType, IBoardReadonly<IPiece> pieces)
    {
        var result = new List<Move>();

        var vectorsToAdd = new List<Vector2Int> {new Vector2Int(1, 2), new Vector2Int(2, 1)};

        var possibleMultipliers = new List<int> {-1, 1};

        foreach (var vectorToAdd in vectorsToAdd)
        {
            foreach (var xMultiplier in possibleMultipliers)
            {
                foreach (var yMultiplier in possibleMultipliers)
                {
                    var boardCoordsToAdd = new Vector2Int(
                        xMultiplier*vectorToAdd.x,
                        yMultiplier*vectorToAdd.y);

                    var boardCoords = pieceBoardCoords + boardCoordsToAdd;

                    if (!pieces.IsOnPlayingField(boardCoords))
                        continue;

                    var pieceOnTargetBoardCoords = pieces.Get(boardCoords);

                    if (pieceOnTargetBoardCoords == null)
                    {
                        result.Add(new Move(player, pieceType, pieceBoardCoords, boardCoords, MoveType.Move));
                    }
                    else if (pieceOnTargetBoardCoords.Player == player.GetOpponent())
                    {
                        result.Add(new Move(player, pieceType, pieceBoardCoords, boardCoords, MoveType.Eat));
                    }
                }
            }
        }
        return result;
    }

    public static bool CanPromotePawn(this Move move)
    {
        if (move.PieceType != ChessPieceType.Pawn)
            return false;

        if (move.Player == Player.White)
            return move.Destination.y == 7;

        else if (move.Player == Player.Black)
            return move.Destination.y == 0;

        return false;
    }

    // perhaps this would be done easier with 2 direction flags?
    private static Vector2Int GetBoardCoordsInDirection(Vector2Int boardCoords, Direction direction, int length = 1)
    {
        switch (direction)
        {
            case Direction.North:
                return new Vector2Int(boardCoords.x, boardCoords.y + length);
            case Direction.Northeast:
                return new Vector2Int(boardCoords.x + length, boardCoords.y + length);
            case Direction.East:
                return new Vector2Int(boardCoords.x + length, boardCoords.y);
            case Direction.Southeast:
                return new Vector2Int(boardCoords.x + length, boardCoords.y - length);
            case Direction.South:
                return new Vector2Int(boardCoords.x, boardCoords.y - length);
            case Direction.Southwest:
                return new Vector2Int(boardCoords.x - 1, boardCoords.y - 1);
            case Direction.West:
                return new Vector2Int(boardCoords.x - 1, boardCoords.y);
            case Direction.Northwest:
                return new Vector2Int(boardCoords.x - 1, boardCoords.y + 1);
            default:
                throw new InvalidEnumArgumentException();
        }
    }
}
