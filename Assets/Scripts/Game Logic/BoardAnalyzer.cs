﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

// should this be static or not?
public static class BoardAnalyzer
{
    public static bool HasAvailableMoves(IBoardReadonly<IPiece> pieces, Player player)
    {
        foreach (var piece in pieces.GetAll().Where(x => x.Player == player))
        {
            var allowedBoardCoords = GetAllowedMoves(piece, pieces);

            if (allowedBoardCoords.Any())
                return true;
        }

        return false;
    }

    // refactor this into something nicer
    public static List<Move> GetKingCastlingMove(IBoardReadonly<IPiece> pieces, IPiece king, Direction direction)
    {
        var emptyList = new List<Move>();

        if (king.HasMoved)
            return emptyList;

        var boardCoords = king.GetBoardCoords();

        // calling this is kind of dumb now because it returns Moves and spurs illogicalities
        var allowedMovesInDirection = BoardCoordsCalculator.GetAllowedMovesInDirection(boardCoords, boardCoords,
            direction, pieces, king.Player, king.ChessPieceType, maxLength: -1, incudeFriendlyRooks: true);

        if (allowedMovesInDirection.Count < 3)
            return emptyList;

        var rookBoardCoordinates = allowedMovesInDirection.FirstOrDefault(x => x.MoveType == MoveType.Castling);

        if (rookBoardCoordinates == null)
            return emptyList;

        // if castling is possible, the destination move is next to last
        var castlingMove = allowedMovesInDirection[allowedMovesInDirection.Count - 2];

        if (castlingMove == null) return emptyList;

        var rookForCastling = pieces.Get(rookBoardCoordinates.Destination);

        if(rookForCastling.HasMoved) return emptyList;

        var rookDestination = allowedMovesInDirection[allowedMovesInDirection.Count - 3].Destination;

        // move type Castling or move type Move for the rook?
        castlingMove.CastlingOtherMove = new Move(king.Player, king.ChessPieceType, rookForCastling.GetBoardCoords(),
            rookDestination, MoveType.Castling);

        castlingMove.MoveType = MoveType.Castling;
        
        return new List<Move> { castlingMove };
    }

    public static bool CanMoveToTile(GameObject obj, GameObject destinationTile, IBoardReadonly<IPiece> pieces)
    {
        if (obj == null || destinationTile == null)
            return false;

        var pieceScript = obj.GetScript<IPiece>();

        var destinationTileScript = destinationTile.GetScript<Tile>();

        var allowedMoves = GetAllowedMoves(pieceScript, pieces);

        return allowedMoves.Values.Any(x => x.Destination == destinationTileScript.XBoardCoords);
    }

    public static IDictionary<Vector2Int, Move> GetAllowedMoves(IPiece piece, IBoardReadonly<IPiece> tiles)
    {
        var result = new Dictionary<Vector2Int, Move>();

        var pieceCoords = piece.GetBoardCoords();

        var allowedMoves = piece.GetAllowedMoves(tiles);

        foreach (var move in allowedMoves)
        {
            var boardAfterMove = tiles.MakeEditableCopy();

            boardAfterMove.Set(move.Destination, piece);

            boardAfterMove.Set(pieceCoords, null);

            if (IsKingInCheck(boardAfterMove, piece.Player))
            {
                continue;
            }

            result[move.Destination] = move;
        }

        return result;
    }
    
    public static bool IsKingInCheck(IBoardReadonly<IPiece> pieces, Player player)
    {
        var opponent = player.GetOpponent();

        var allAttackedCoords = GetAllAttackedBoardCoordsBy(pieces, opponent);

        return ContainsKing(pieces, allAttackedCoords, player);
    }

    private static List<Vector2Int> GetAllAttackedBoardCoordsBy(IBoardReadonly<IPiece> pieces, Player player)
    {
        var result = new List<Vector2Int>();

        var playerPieces = pieces.GetAll().Where(x => x.Player == player);

        foreach (var piece in playerPieces)
        {
            var attackedBoardCoords = piece
                .GetAllowedMoves(pieces)
                .Select(x => x.Destination);

            result = result.Union(attackedBoardCoords).ToList();
        }

        return result;
    }

    private static bool ContainsKing(IBoardReadonly<IPiece> board, List<Vector2Int> boardCoordsToCheck, Player player)
    {
        foreach (var boardCoords in boardCoordsToCheck)
        {
            var piece = board.Get(boardCoords);

            if (piece == null) continue;
            
            if (piece.ChessPieceType == ChessPieceType.King && piece.Player == player)
                return true;
        }

        return false;
    }
}
