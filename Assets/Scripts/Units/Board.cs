﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Board<T> : IBoard<T>
{
    // matrix starts from lower left corner of white player - A1 field
    // X is column, Y is row
    private readonly List<List<T>> _tiles;

    private readonly int _boardSize;

    private readonly Dictionary<Vector2Int, T> _tilesAsDict;

    public int Size {
        get
        {
            return _boardSize;
        }
    }

    public Board(int size)
    {
        _boardSize = size;

        _tiles = new List<List<T>>(size);

        _tilesAsDict =  new Dictionary<Vector2Int, T>(size * size);

        for (var xCoord = 0; xCoord < size; xCoord++)
        {
            var col = new List<T>();

            for (var yCoord = 0; yCoord < size; yCoord++)
            {
                var boardCoords = new Vector2Int(xCoord, yCoord);

                var value = default(T);

                col.Add(default(T));

                _tilesAsDict[boardCoords] = value;
            }

            _tiles.Add(col);
        }
    }

    public T Get(Vector2Int boardCoords)
    {
        // the default thing is probably stupid
        if (!IsOnPlayingField(boardCoords))
            return default(T);

        return _tiles[boardCoords.x][boardCoords.y];
    }

    public List<T> GetAll()
    {
        return _tilesAsDict.Values.Where(
            x => 
            x != null
            && !x.Equals(default(T)))
            .ToList();
    }

    public void Set(Vector2Int boardCoords, T value)
    {
        if (!IsOnPlayingField(boardCoords))
        {
            throw new Exception("Board coordinates " + boardCoords + " are out of range. Board size is " + _boardSize);
        }

        _tiles[boardCoords.x][boardCoords.y] = value;

        _tilesAsDict[boardCoords] = value;
    }

    public Board<T> MakeEditableCopy()
    {
        var other = new Board<T>(Size);

        for (var i = 0; i < Size; i++)
        {
            for (var j = 0; j < Size; j++)
            {
                var boardCoords = new Vector2Int(i, j);

                var value = Get(boardCoords);

                other.Set(boardCoords, value);
            }
        }

        return other;
    }

    public bool IsOnPlayingField(Vector2Int boardCoords)
    {
        if (boardCoords.x < 0 || boardCoords.y < 0)
            return false;

        if (boardCoords.x > _tiles.Count - 1)
            return false;

        if (boardCoords.y > _tiles[boardCoords.x].Count - 1)
            return false;

        return true;
    }
}
