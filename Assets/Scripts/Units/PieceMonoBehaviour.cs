﻿using System.Collections.Generic;
using UnityEngine;

public abstract class PieceMonoBehaviour : MonoBehaviour, IPiece {

    // these are here for debugging from the Unity editor
    public GameObject XCurrentTile;
    public Player XPlayer;
    public bool XHasMoved = false;
    public bool XIsInPlay;
    
    public abstract ChessPieceType ChessPieceType { get; }

    public Player Player
    {
        get { return XPlayer; }
        set { XPlayer = value; }
    }

    public GameObject CurrentTile
    {
        get { return XCurrentTile; }
        set { XCurrentTile = value; }
    }

    public bool HasMoved
    {
        get { return XHasMoved; }
        set { XHasMoved = value; }
    }

    public bool IsInPlay
    {
        get { return XIsInPlay; }
        set { XIsInPlay = value; }
    }

    public abstract List<Move> GetAllowedMoves(IBoardReadonly<IPiece> pieces);

    public void Mark()
    {
        var selectSkin = GetSelectSkin();

        var defaultSkin = GetDefaultSkin();

        selectSkin.SetActive(true);

        defaultSkin.SetActive(false);
    }

    public void Unmark()
    {
        var selectSkin = GetSelectSkin();

        var defaultSkin = GetDefaultSkin();

        selectSkin.SetActive(false);

        defaultSkin.SetActive(true);
    }

    private GameObject GetDefaultSkin()
    {
        var selectSkin = transform.Find(Names.Piece);

        return selectSkin.gameObject;
    }

    private GameObject GetSelectSkin()
    {
        var selectSkin = transform.Find(Names.Selected);

        return selectSkin.gameObject;
    }
}
