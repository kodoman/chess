﻿using System.Collections.Generic;

public class Bishop : PieceMonoBehaviour
{
    public override ChessPieceType ChessPieceType
    {
        get { return ChessPieceType.Bishop; }
    }
    
    public override List<Move> GetAllowedMoves(IBoardReadonly<IPiece> pieces)
    {
        var result = new List<Move>();

        var myBoardCoords = this.GetBoardCoords();

        result.AddRange(BoardCoordsCalculator.GetAllowedMovesInDirection(myBoardCoords, myBoardCoords, Direction.Northeast, 
            pieces, Player, ChessPieceType));

        result.AddRange(BoardCoordsCalculator.GetAllowedMovesInDirection(myBoardCoords, myBoardCoords, Direction.Northwest, 
            pieces, Player, ChessPieceType));

        result.AddRange(BoardCoordsCalculator.GetAllowedMovesInDirection(myBoardCoords, myBoardCoords, Direction.Southeast,
            pieces, Player, ChessPieceType));

        result.AddRange(BoardCoordsCalculator.GetAllowedMovesInDirection(myBoardCoords, myBoardCoords, Direction.Southwest,
            pieces, Player, ChessPieceType));

        return result;
    }
}
