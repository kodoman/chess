﻿using System.Collections.Generic;

public class Pawn : PieceMonoBehaviour
{
    public override ChessPieceType ChessPieceType
    {
        get { return ChessPieceType.Pawn; }
    }
    
    public override List<Move> GetAllowedMoves(IBoardReadonly<IPiece> pieces)
    {
        var result = new List<Move>();

        var myBoardCoords = this.GetBoardCoords();

        var forwardDirection = 
            Player == Player.White 
            ? Direction.North 
            : Direction.South;

        var forwardWestDirection = 
            Player == Player.White
            ? Direction.Northwest
            : Direction.Southwest;

        var forwardEastDirection =
            Player == Player.White
            ? Direction.Northeast
            : Direction.Southeast;

        var maxForwardMovement = HasMoved ? 1 : 2;

        result.AddRange(BoardCoordsCalculator.GetAllowedMovesInDirection(myBoardCoords, myBoardCoords, forwardDirection,
            pieces, Player, ChessPieceType, maxForwardMovement, disallowEnemyFields: true));

        result.AddRange(BoardCoordsCalculator.GetAllowedMovesInDirection(myBoardCoords, myBoardCoords, forwardWestDirection,
            pieces, Player, ChessPieceType, 1, ignoreEmptyFields: true));

        result.AddRange(BoardCoordsCalculator.GetAllowedMovesInDirection(myBoardCoords, myBoardCoords, forwardEastDirection,
            pieces, Player, ChessPieceType, 1, ignoreEmptyFields: true));

        return result;
    }
}
