﻿using System.Collections.Generic;

public class Rook : PieceMonoBehaviour
{
    public override ChessPieceType ChessPieceType
    {
        get { return ChessPieceType.Rook; }
    }

    public override List<Move> GetAllowedMoves(IBoardReadonly<IPiece> pieces)
    {
        var result = new List<Move>();

        var myBoardCoords = this.GetBoardCoords();

        result.AddRange(BoardCoordsCalculator.GetAllowedMovesInDirection(myBoardCoords, myBoardCoords, Direction.North,
            pieces, Player, ChessPieceType));
        result.AddRange(BoardCoordsCalculator.GetAllowedMovesInDirection(myBoardCoords, myBoardCoords, Direction.East,
            pieces, Player, ChessPieceType));
        result.AddRange(BoardCoordsCalculator.GetAllowedMovesInDirection(myBoardCoords, myBoardCoords, Direction.South,
            pieces, Player, ChessPieceType));
        result.AddRange(BoardCoordsCalculator.GetAllowedMovesInDirection(myBoardCoords, myBoardCoords, Direction.West,
            pieces, Player, ChessPieceType));

        return result;
    }
}
