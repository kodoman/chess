﻿using System.Collections.Generic;

public class Knight : PieceMonoBehaviour {

    public override ChessPieceType ChessPieceType
    {
        get { return ChessPieceType.Knight; }
    }
    
    public override List<Move> GetAllowedMoves(IBoardReadonly<IPiece> pieces)
    {
        var myBoardCoords = this.GetBoardCoords();

        var result = BoardCoordsCalculator.GetAllowedLShapedMoves(myBoardCoords, Player, ChessPieceType, pieces);

        return result;
    }
}
