﻿using System;
using System.Collections.Generic;

public class Queen : PieceMonoBehaviour
{
    public override ChessPieceType ChessPieceType
    {
        get { return ChessPieceType.Queen; }
    }

    public override List<Move> GetAllowedMoves(IBoardReadonly<IPiece> pieces)
    {
        var result = new List<Move>();

        var myBoardCoords = this.GetBoardCoords();

        foreach (Direction direction in Enum.GetValues(typeof(Direction)))
        {
            result.AddRange(BoardCoordsCalculator.GetAllowedMovesInDirection(myBoardCoords, myBoardCoords, direction,
               pieces, Player, ChessPieceType));
        }

        return result;
    }
}
