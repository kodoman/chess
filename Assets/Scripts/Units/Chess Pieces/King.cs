﻿using System;
using System.Collections.Generic;

public class King : PieceMonoBehaviour
{
    public override ChessPieceType ChessPieceType
    {
        get { return ChessPieceType.King; }
    }

    public override List<Move> GetAllowedMoves(IBoardReadonly<IPiece> pieces)
    {
        var result = new List<Move>();

        var myBoardCoords = this.GetBoardCoords();

        foreach (Direction direction in Enum.GetValues(typeof(Direction)))
        {
            result.AddRange(BoardCoordsCalculator.GetAllowedMovesInDirection(myBoardCoords, myBoardCoords, direction,
               pieces, Player, ChessPieceType, maxLength: 1));
        }

        // how to mark these as special? The rook should also be moved when clicking
        result.AddRange(BoardAnalyzer.GetKingCastlingMove(pieces, this, Direction.East));
        result.AddRange(BoardAnalyzer.GetKingCastlingMove(pieces, this, Direction.West));

        return result;
    }
}
