﻿using UnityEngine;

public class Move
{
    public Move(Player player, ChessPieceType pieceType, Vector2Int source, Vector2Int destination, MoveType moveType = MoveType.Move)
    {
        Player = player;
        PieceType = pieceType;
        Source = source;
        Destination = destination;
        MoveType = moveType;
    }

    public Player Player;
    public ChessPieceType PieceType;

    public MoveType MoveType;

    public Vector2Int Source;
    public Vector2Int Destination;
    
    public Move CastlingOtherMove;
}
