﻿using UnityEngine;

public class TileObjectBoard : Board<GameObject>
{
    public TileObjectBoard(int size) : base(size)
    {
    }

    // this may be unoptimized - try profiling this to see exactly how much
    public IBoardReadonly<IPiece> PieceBoard
    {
        get
        {
            var board = new Board<IPiece>(Size);

            foreach (var boardObject in GetAll())
            {
                var tile = boardObject.GetScript<Tile>();
                board.Set(tile.XBoardCoords, tile.GetPieceOnTile());
            }

            return board;
        }
    }

    public Tile GetTile(Vector2Int boardCoords)
    {
        var tile = Get(boardCoords);

        if (tile == null) return null;

        return tile.GetScript<Tile>();
    }
}
