﻿using UnityEngine;

public class Tile : MonoBehaviour
{
    public float TileHeight = 0;

    public GameObject XUnitOnTile;
    public Vector2Int XBoardCoords;
    public GameObject SelectMarker;
    public GameObject EatMarker;
    public GameObject CastlingMarker;

    private bool _isMarked = false;

    public IPiece GetPieceOnTile()
    {
        return XUnitOnTile == null 
            ? null 
            : XUnitOnTile.GetScript<IPiece>();
    }

    public void SetMarked(bool marked, MoveType moveType = MoveType.Move)
    {
        if (_isMarked == marked)
            return;

        _isMarked = marked;

        DeactivateAllMarkers();

        if (!marked)
            return;

        switch (moveType)
        {
            case MoveType.Move:
                SelectMarker.SetActive(true);
                break;
            case MoveType.Castling:
                CastlingMarker.SetActive(true);
                break;
            case MoveType.Eat:
                EatMarker.SetActive(true);
                break;
        }
    }

    private void DeactivateAllMarkers()
    {
        if (SelectMarker.activeSelf)
            SelectMarker.SetActive(false);

        if (EatMarker.activeSelf)
            EatMarker.SetActive(false);

        if (CastlingMarker.activeSelf)
            CastlingMarker.SetActive(false);
    }
}
